# workout-notifier

This CLI tool uses desktop notifications to post a random workout on a duration that can be consistent or random.

```bash
# Post a workout every hour
./workout-notifier -h=1

# Post a workout every 5 minutes
./workout-notifier -m=5

# Post a workout at a random number
# between 1 and 5 hours and a random
# number between 1 and 30 minutes.
# After the workout posts, the iteration
# is randomized again.
./workout-notifier -h=5 -m=30 -r

# Background the process and forget about it
./workout-notifier -h=8 &

# Help
./workout-notifier
```

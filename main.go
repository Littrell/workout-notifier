package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"time"

	"github.com/gen2brain/beeep"
	"gopkg.in/yaml.v3"
)

type Workout struct {
	Name string `yaml:"name"`
	Min  int    `yaml:"min"`
	Max  int    `yaml:"max"`
	Skip bool   `yaml:"skip"`
}

type WorkoutYaml struct {
	Workouts []Workout `yaml:"workouts"`
}

func main() {
	yml, err := ioutil.ReadFile("workout.yml")
	if err != nil {
		panic("A workout.yml file is required")
	}

	w := WorkoutYaml{}
	err = yaml.Unmarshal(yml, &w)
	if err != nil {
		panic(err)
	}

	// TODO check for case where all workouts are skip

	err = beeep.Notify("workout-notifier", "Starting workout-notifier", "")
	if err != nil {
		panic(err)
	}

	workouts := w.Workouts

	rand.Seed(time.Now().UnixNano())

	h := flag.Int("h", 0, "Hours; Default is 0")
	m := flag.Int("m", 0, "Minutes; Default is 0")
	r := flag.Bool("r", true, "Randomize the number of hours and minutes")

	flag.Parse()

	if *h == 0 && *m == 0 {
		flag.PrintDefaults()
		os.Exit(0)
	}

	for {
		var d time.Duration

		if *h != 0 {
			if *r {
				rh := rand.Intn(*h)
				if rh == 0 {
					rh = 1
				}
				d = time.Hour * time.Duration(rh)
			} else {
				d = time.Hour * time.Duration(*h)
			}
		}

		if *m != 0 {
			if *r {
				rm := rand.Intn(*m)
				if rm == 0 {
					rm = 1
				}
				d = d + time.Minute*time.Duration(rm)
			} else {
				d = d + time.Minute*time.Duration(*m)
			}
		}

		time.Sleep(d)

		var workout Workout
		for {
			rwi := rand.Intn(len(workouts))
			workout = workouts[rwi]
			if !workout.Skip {
				break
			}
		}

		var rep int
		if workout.Max != workout.Min {
			rep = rand.Intn(workout.Max-workout.Min) + workout.Min
		} else {
			rep = workout.Min
		}

		err := beeep.Notify("workout-notifier - Time to work out!", fmt.Sprintf("%d %s", rep, workout.Name), "")
		if err != nil {
			panic(err)
		}
	}
}
